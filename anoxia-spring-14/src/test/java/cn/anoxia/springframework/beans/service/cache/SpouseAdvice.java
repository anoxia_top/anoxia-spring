package cn.anoxia.springframework.beans.service.cache;

import cn.anoxia.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * The class SpouseAdvice.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/09/02 11:32
 */
public class SpouseAdvice implements MethodBeforeAdvice {

  @Override
  public void before(Method method, Object[] args, Object target) throws Throwable {
    System.out.println("关怀小两口(切面)：" + method);
  }

}