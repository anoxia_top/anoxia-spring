package cn.anoxia.springframework.beans.service.bean;

import cn.anoxia.springframework.beans.dao.IUserDao;
import cn.anoxia.springframework.beans.factory.FactoryBean;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 * The class ProxyBeanFactory.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 17:00
 */
public class ProxyBeanFactory implements FactoryBean<IUserDao> {

  @Override
  public IUserDao getObject() throws Exception {
    InvocationHandler handler = (proxy,method,args)->{
      Map<String, String> hashMap = new HashMap<>();
      hashMap.put("001","Anoxia");
      hashMap.put("002","xiaoming");
      return "你被代理了"+ method.getName()+":"+hashMap.get(args[0].toString());
    };
    return (IUserDao) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{IUserDao.class}, handler);
  }

  @Override
  public Class<?> getObjectType() {
    return IUserDao.class;
  }

  @Override
  public boolean isSingleton() {
    return true;
  }
}
