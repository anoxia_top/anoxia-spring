package cn.anoxia.springframework.beans.service.cache;

/**
 * The class IMother.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/09/02 11:30
 */
public interface IMother {

  String callMother();

}
