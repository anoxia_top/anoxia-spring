package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import org.junit.jupiter.api.Test;

/**
 * The class ScanTest.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 15:28
 */
public class ScanTest {

  @Test
  public void test_property() throws BeansException {
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring-property.xml");
    IUserService userService = applicationContext.getBean("userService", IUserService.class);
    System.out.println("测试结果：" + userService.queryUserInfo());
  }

  @Test
  public void test_scan() throws BeansException {
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring-scan.xml");
    IUserService userService = applicationContext.getBean("userService", IUserService.class);
    System.out.println("测试结果："+userService.queryUserInfo());
  }

}
