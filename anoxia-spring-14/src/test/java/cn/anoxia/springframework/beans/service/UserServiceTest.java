package cn.anoxia.springframework.beans.service;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.dao.IUserDao;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import cn.hutool.core.util.StrUtil;
import org.junit.jupiter.api.Test;
import org.openjdk.jol.info.ClassLayout;
import sun.security.util.Length;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.PriorityQueue;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
class UserServiceTest {

  @Test
  public void test_prototype() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    System.out.println(applicationContext.getBean("proxyUserDao").toString());

    // 2. 获取Bean对象调用方法
    UserService userService1 = applicationContext.getBean("userService", UserService.class);
    UserService userService2 = applicationContext.getBean("userService", UserService.class);

    System.out.println(userService1);
    System.out.println(userService2);

    System.out.println(userService1+"十六进制哈希："+Integer.toHexString(Integer.parseInt(String.valueOf(userService1.hashCode()))));
    System.out.println(ClassLayout.parseInstance(userService1).toPrintable());
  }

  @Test
  public void test_proxy() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    UserService userService = applicationContext.getBean("userService", UserService.class);
    System.out.println(userService.queryUserInfo());
  }

  @Test
  public void test_proxy_class(){
    IUserDao instance = (IUserDao) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{IUserDao.class}, (proxy, method, args) -> "你被代理了");
    System.out.println("测试结果："+instance.queryUserName("001"));

  }

  @Test
  public void test_sort(){
    int[] num1 = {1,10,4,11};
    int[] num2 = {2,7,11,15};
    int[] ints = sortDemo(num2, num1);
    for (int num : ints){
      System.out.println(num);
    }
  }

  private int[] sortDemo(int[] num1,int[] num2){
    int n = num1.length;
    // 用于冲排序，最后返回
    int[] res = new int[n];
    // 排序需要比对的数组,获取到第一个数组最大值和最小值
    Arrays.sort(num1);
    // 大根堆，选出第二个数组的最大最小值
    PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->b[0]-a[0]);
    for (int i = 0; i < n; i++) {
      pq.add(new int[]{num2[i], i});
    }
    // 双指针遍历
      int left = 0;
      int rigth = n-1;
      while (!pq.isEmpty()){
        // 出栈
        int[] cur = pq.poll();
        // 获取下标
        int index = cur[1];
        // 获取值
        int val = cur[0];
        // 如果第一个数组的最小值，大于当前值，用最小的更当前最大的比
        if (num1[rigth] > val){
          res[index] = num1[rigth--];
        }else {
          res[index] = num1[left++];
        }
      }
    return res;
  }


}