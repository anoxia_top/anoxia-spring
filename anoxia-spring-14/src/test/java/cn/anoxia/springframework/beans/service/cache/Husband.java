package cn.anoxia.springframework.beans.service.cache;

/**
 * The class Husband.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/09/02 11:28
 */
public class Husband {

  private Wife wife;

  public String queryWife(){
    return "wife";
  }

  public Wife getWife() {
    return wife;
  }

  public void setWife(Wife wife) {
    this.wife = wife;
  }
}
