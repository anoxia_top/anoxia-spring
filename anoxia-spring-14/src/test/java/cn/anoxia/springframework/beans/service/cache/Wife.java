package cn.anoxia.springframework.beans.service.cache;

import cn.anoxia.springframework.beans.service.cache.Husband;

import javax.swing.*;

/**
 * The class Wife.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/09/02 11:28
 */
public class Wife {

  private Husband husband;
  private IMother mother; // 婆婆

  public String queryHusband() {
    return "Wife.husband、.callMother："+mother.callMother();
  }

  public Husband getHusband() {
    return husband;
  }

  public void setHusband(Husband husband) {
    this.husband = husband;
  }

  public IMother getMother() {
    return mother;
  }

  public void setMother(IMother mother) {
    this.mother = mother;
  }
}
