package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.service.cache.Husband;
import cn.anoxia.springframework.beans.service.cache.Wife;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import org.junit.jupiter.api.Test;

/**
 * The class CacheTest.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/09/02 13:41
 */
public class CacheTest {

  @Test
  public void test_cache() throws BeansException {
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    Husband husband = applicationContext.getBean("husband", Husband.class);
    Wife wife = applicationContext.getBean("wife", Wife.class);
    IUserService userService = applicationContext.getBean("userService",UserService.class);
    System.out.println(userService.queryUserInfo());
    System.out.println("wife：" + husband.queryWife());
    System.out.println("husband：" + wife.queryHusband());
  }

}
