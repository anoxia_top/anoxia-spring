/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.factory.config.BeanFactoryPostProcessor;
import cn.anoxia.springframework.beans.factory.config.BeanPostProcessor;
import cn.anoxia.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import cn.anoxia.springframework.context.ApplicationEvent;
import cn.anoxia.springframework.context.ApplicationListener;
import cn.anoxia.springframework.context.ConfigurableApplicationContext;
import cn.anoxia.springframework.context.event.ApplicationEventMulticaster;
import cn.anoxia.springframework.context.event.ContextClosedEvent;
import cn.anoxia.springframework.context.event.ContextRefreshedEvent;
import cn.anoxia.springframework.context.event.SimpleApplicationEventMulticaster;
import cn.anoxia.springframework.context.support.ApplicationContextAwareProcessor;
import cn.anoxia.springframework.core.io.DefaultResourceLoader;

import java.util.Collection;
import java.util.Map;

/**
 * @author huangle
 */
public abstract class AbstractApplicationContext extends DefaultResourceLoader implements ConfigurableApplicationContext {


  public static final String APPLICATION_EVENT_MULTICASTER_BEAN_NAME = "applicationEventMulticaster";

  private ApplicationEventMulticaster applicationEventMulticaster;


  @Override
  public void refresh() throws BeansException {
    // 1、创建beanfactory
    refreshBeanFactory();

    // 2 获取beanfactory
    ConfigurableListableBeanFactory beanFactory = getBeanFactory();

    // 3. 添加 ApplicationContextAwareProcessor，让继承自 ApplicationContextAware 的 Bean 对象都能感知所属的 ApplicationContext
    beanFactory.addBeanPostProcessor(new ApplicationContextAwareProcessor(this));

    // 4. 在 Bean 实例化之前，执行 BeanFactoryPostProcessor (Invoke factory processors registered as beans in the context.)
    invokeBeanFactoryPostProcessors(beanFactory);

    // 5. BeanPostProcessor 需要提前于其他 Bean 对象实例化之前执行注册操作
    registerBeanPostProcessors(beanFactory);

    // 6. 初始化事件发布者
    initApplicationEventMulticaster();

    // 7. 注册事件监听器
    registerListeners();

    // 6. 提前实例化单例Bean对象
    beanFactory.preInstantiateSingletons();

    finishRefresh();
  }

  private void finishRefresh() throws BeansException {
    publishEvent(new ContextRefreshedEvent(this));
  }

  public void publishEvent(ApplicationEvent event) throws BeansException {
    applicationEventMulticaster.multicastEvent(event);
  }

  private void registerListeners() throws BeansException {
    Collection<ApplicationListener> applicationListeners = getBeansOfType(ApplicationListener.class).values();
    for (ApplicationListener listener : applicationListeners) {
      applicationEventMulticaster.addApplicationListener(listener);
    }
  }

  private void initApplicationEventMulticaster() {
    // 获取beanfactory
    ConfigurableListableBeanFactory beanFactory = getBeanFactory();
    // 初始化事件
    applicationEventMulticaster = new SimpleApplicationEventMulticaster(beanFactory);
    // 注册事件,初始化对象给容器
    beanFactory.registerSingleton(APPLICATION_EVENT_MULTICASTER_BEAN_NAME, applicationEventMulticaster);
  }

  protected abstract void refreshBeanFactory() throws BeansException;

  protected abstract ConfigurableListableBeanFactory getBeanFactory();

  private void invokeBeanFactoryPostProcessors(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    Map<String, BeanFactoryPostProcessor> beanFactoryPostProcessorMap = beanFactory.getBeansOfType(BeanFactoryPostProcessor.class);
    for (BeanFactoryPostProcessor beanFactoryPostProcessor : beanFactoryPostProcessorMap.values()) {
      beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);
    }
  }

  private void registerBeanPostProcessors(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    Map<String, BeanPostProcessor> beanPostProcessorMap = beanFactory.getBeansOfType(BeanPostProcessor.class);
    for (BeanPostProcessor beanPostProcessor : beanPostProcessorMap.values()) {
      beanFactory.addBeanPostProcessor(beanPostProcessor);
    }
  }

  @Override
  public String[] getBeanDefinitionNames() {
    return getBeanFactory().getBeanDefinitionNames();
  }

  @Override
  public Object getBean(String name) throws BeansException {
    return getBeanFactory().getBean(name);
  }

  @Override
  public Object getBean(String name, Object... args) throws BeansException {
    return getBeanFactory().getBean(name,args);
  }

  @Override
  public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
    return getBeanFactory().getBean(name, requiredType);
  }

  @Override
  public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
    return getBeanFactory().getBeansOfType(type);
  }

  @Override
  public <T> T getBean(Class<T> requiredType) throws BeansException {
    return getBeanFactory().getBean(requiredType);
  }

  @Override
  public void registerShutdownHook() {
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      try {
        close();
      } catch (BeansException e) {
        e.printStackTrace();
      }
    }));
  }

  @Override
  public void close() throws BeansException {
    // 发布容器关闭事件
    publishEvent(new ContextClosedEvent(this));
    getBeanFactory().destroySingletons();
  }
}
