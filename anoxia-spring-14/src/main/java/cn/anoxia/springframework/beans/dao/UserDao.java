/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.dao;

import cn.anoxia.springframework.context.annotation.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huangle
 */
@Component("userDao")
public class UserDao {

  private static Map<String,String> map = new HashMap<>();

  static {
    map.put("001","Anoxia");
    map.put("002","Xiaoming");
  }

  public void initDataMethod(){
    System.out.println("执行：init-method");
    map.put("001","Anoxia");
    map.put("002","xiaoming");
  }

  public void destroyDataMethod(){
    System.out.println("执行：destroy-method");
    map.clear();
  }

  public String queryUserName(String id){
    return map.get(id);
  }

}
