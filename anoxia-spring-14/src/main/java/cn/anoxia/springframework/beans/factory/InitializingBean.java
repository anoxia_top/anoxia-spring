package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.BeansException;

/**
 * The class InitializingBean.
 * <p>
 * Description: 初始化方法
 *
 * @author: huangle
 * @since: 2021/08/27 10:29
 */
public interface InitializingBean {


  /**
   * @Description: 初始化
   * @author huangle
   * @Date 2021/8/27-14:03
   * @param
   * @return: void
   */
  void afterPropertiesSet() throws BeansException;

}
