package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.Aware;

/**
 * The class BeanNameAware.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 14:39
 */
public interface BeanNameAware extends Aware {

  void setBeanName(String name);

}
