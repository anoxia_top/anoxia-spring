/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.factory.xml;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.factory.BeanReference;
import cn.anoxia.springframework.beans.support.AbstractBeanDefinitionReader;
import cn.anoxia.springframework.beans.support.BeanDefinitionRegistry;
import cn.anoxia.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import cn.anoxia.springframework.core.io.Resource;
import cn.anoxia.springframework.core.io.ResourceLoader;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.XmlUtil;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.BeanDefinitionStoreException;
import org.springframework.util.Assert;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.List;

public class XmlBeanDefinitionReader extends AbstractBeanDefinitionReader {

  public XmlBeanDefinitionReader(BeanDefinitionRegistry registry){
    super(registry);
  }

  public XmlBeanDefinitionReader(BeanDefinitionRegistry registry, ResourceLoader resourceLoader) {
    super(registry, resourceLoader);
  }

  @Override
  public void loadBeanDefinitions(Resource resource) throws BeansException {

    try {
      try(InputStream inputStream = resource.getInputStream()){
        doLoadBeanDefinitions(inputStream);
      }
    }catch (Exception e){
      throw new BeansException("解析文件xml 获取inputstream流失败 " + resource, e);
    }

  }

  @Override
  public void loadBeanDefinitions(Resource... resources) throws BeansException {
    for (Resource resource: resources){
      loadBeanDefinitions(resource);
    }
  }

  @Override
  public void loadBeanDefinitions(String location) throws BeansException {
    ResourceLoader resourceLoader = getResourceLoader();
    Resource resource = resourceLoader.getResource(location);
    loadBeanDefinitions(resource);
  }

  @Override
  public void loadBeanDefinitions(String... locations) throws BeanDefinitionStoreException, BeansException {
    Assert.notNull(locations, "Location array must not be null");
    for (String location : locations) {
      loadBeanDefinitions(location);
    }
  }

  protected void doLoadBeanDefinitions(InputStream inputStream) throws ClassNotFoundException, BeansException, DocumentException {

    SAXReader reader = new SAXReader();
    Document document = reader.read(inputStream);
    Element root = document.getRootElement();
    Element componentScan = root.element("component-scan");
    if (null != componentScan){
      String scanPath = componentScan.attributeValue("base-package");
      if (StrUtil.isEmpty(scanPath)){
        throw new BeansException("扫描包的根目录不存在");
      }
      scanPackage(scanPath);
    }
    List<Element> beanList = root.elements("bean");
    for (Element bean : beanList){
      String id = bean.attributeValue("id");
      String name = bean.attributeValue("name");
      String className = bean.attributeValue("class");
      String initMethodName = bean.attributeValue("init-method");
      String destroyMethodName = bean.attributeValue("destroy-method");
      String beanScope = bean.attributeValue("scope");

      // 获取 Class，方便获取类中的名称
      Class<?> clazz = Class.forName(className);
      // 优先级 id > name
      String beanName = StrUtil.isNotEmpty(id) ? id : name;
      if (StrUtil.isEmpty(beanName)) {
        beanName = StrUtil.lowerFirst(clazz.getSimpleName());
      }
      // 定义Bean
      BeanDefinition beanDefinition = new BeanDefinition(clazz);

      // 设置bean模式（单例，原型）
      if (StrUtil.isNotBlank(beanScope)){
        beanDefinition.setScope(beanScope);
      }

      // 设置初始化方法和销毁方法
      if (StrUtil.isNotBlank(initMethodName)){
        beanDefinition.setInitMethodName(initMethodName);
      }
      if (StrUtil.isNotBlank(destroyMethodName)){
        beanDefinition.setDestroyMethodName(destroyMethodName);
      }

      List<Element> propertyList = bean.elements("property");
      for (Element property : propertyList){
        String attrName = property.attributeValue("name");
        String attrValue = property.attributeValue("value");
        String attrRef = property.attributeValue("ref");
        // 获取属性值：引入对象、值对象
        Object value = StrUtil.isNotEmpty(attrRef) ? new BeanReference(attrRef) : attrValue;
        // 创建属性信息
        PropertyValue propertyValue = new PropertyValue(attrName, value);
        beanDefinition.getPropertyValues().addPropertyValue(propertyValue);
      }

      if (getRegistry().containsBeanDefinition(beanName)) {
        throw new BeansException("Duplicate beanName[" + beanName + "] is not allowed");
      }
      // 注册 BeanDefinition
      getRegistry().registerBeanDefinition(beanName, beanDefinition);
    }
  }

  private void scanPackage(String scanPath){
    String[] basePackages = StrUtil.splitToArray(scanPath, ',');
    ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(getRegistry());
    scanner.doScan(basePackages);
  }
}
