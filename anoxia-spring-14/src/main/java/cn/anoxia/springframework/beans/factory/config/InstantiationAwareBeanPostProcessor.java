package cn.anoxia.springframework.beans.factory.config;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValues;

/**
 * The class InstantiationAwareBeanPostProcessor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:59
 */
public interface InstantiationAwareBeanPostProcessor extends BeanPostProcessor {

  /**
   * Apply this BeanPostProcessor <i>before the target bean gets instantiated</i>.
   * The returned bean object may be a proxy to use instead of the target bean,
   * effectively suppressing default instantiation of the target bean.
   *
   * 在 Bean 对象执行初始化方法之前，执行此方法
   *
   * @param beanClass
   * @param beanName
   * @return
   * @throws BeansException
   */
  Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException;


  /**
   *
   * <p>
   * 在 Bean 对象执行初始化方法之后，执行此方法
   *
   * @param bean
   * @param beanName
   * @return
   * @throws BeansException
   */
  boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException;

  /**
   * @Description: 解析注解、设置数据
   * @author huangle
   * @Date 2021/9/1-17:05
   * @param pvs
   * @param bean
   * @param beanName
   * @return: cn.anoxia.springframework.beans.PropertyValues
   */
  PropertyValues postProcessPropertyValues(PropertyValues pvs, Object bean, String beanName) throws BeansException;

  /**
   * 在 Spring 中由 SmartInstantiationAwareBeanPostProcessor#getEarlyBeanReference 提供
   * @param bean
   * @param beanName
   * @return
   */
  default Object getEarlyBeanReference(Object bean, String beanName) throws BeansException {
    return bean;
  }
}
