/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.PropertyValues;

/**
 * @author huangle
 */
public class BeanDefinition {

  String SCOPE_SINGLETON = ConfigurableBeanFactory.SCOPE_SINGLETON;

  String SCOPE_PROTOTYPE = ConfigurableBeanFactory.SCOPE_PROTOTYPE;


  // 类基本信息
  private Class beanClass;

  // 类参数信息
  private PropertyValues propertyValues;

  private String initMethodName;

  private String destroyMethodName;

  private String scope = SCOPE_SINGLETON;

  private boolean singleton = true;

  private boolean prototype = false;



  public BeanDefinition(Class beanClass){
    // 自动创建一个参数空的参数对象
    this(beanClass,null);
  }

  public BeanDefinition(Class beanClass,PropertyValues propertyValues){
    this.beanClass = beanClass;
    this.propertyValues = propertyValues != null ? propertyValues : new PropertyValues();
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public boolean isSingleton() {
    return SCOPE_SINGLETON.equals(this.scope);
  }

  public void setSingleton(boolean singleton) {
    this.singleton = singleton;
  }

  public boolean isPrototype() {
    return prototype;
  }

  public void setPrototype(boolean prototype) {
    this.prototype = prototype;
  }

  public String getInitMethodName() {
    return initMethodName;
  }

  public void setInitMethodName(String initMethodName) {
    this.initMethodName = initMethodName;
  }

  public String getDestroyMethodName() {
    return destroyMethodName;
  }

  public void setDestroyMethodName(String destroyMethodName) {
    this.destroyMethodName = destroyMethodName;
  }

  public Class getBeanClass() {
    return beanClass;
  }

  public void setBeanClass(Class beanClass) {
    this.beanClass = beanClass;
  }

  public PropertyValues getPropertyValues() {
    return propertyValues;
  }

  public void setPropertyValues(PropertyValues propertyValues) {
    this.propertyValues = propertyValues;
  }


}
