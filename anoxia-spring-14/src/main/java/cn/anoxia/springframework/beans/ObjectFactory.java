package cn.anoxia.springframework.beans;

/**
 * The class ObjectFactory.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/09/02 14:05
 */
public interface ObjectFactory<T> {

  T getObject() throws BeansException;

}
