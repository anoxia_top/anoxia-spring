package cn.anoxia.springframework.aop.framework;

import cn.anoxia.springframework.aop.AdvisedSupport;

/**
 * The class ProxyFactory.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:53
 */
public class ProxyFactory {

  private AdvisedSupport advisedSupport;

  public ProxyFactory(AdvisedSupport advisedSupport){
    this.advisedSupport = advisedSupport;
  }

  public Object getProxy(){
    return createAopProxy().getProxy();
  }

  private AopProxy createAopProxy(){
    if (advisedSupport.isProxyTargetClass()){
      return new Cglib2AopProxy(advisedSupport);
    }
    return new JdkDynamicAopProxy(advisedSupport);
  }

}
