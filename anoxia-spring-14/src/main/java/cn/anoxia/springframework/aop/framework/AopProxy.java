package cn.anoxia.springframework.aop.framework;

/**
 * The class AopProxy.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 17:55
 */
public interface AopProxy {

  Object getProxy();

}
