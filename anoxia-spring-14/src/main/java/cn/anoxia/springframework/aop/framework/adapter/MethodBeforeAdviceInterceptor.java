package cn.anoxia.springframework.aop.framework.adapter;

import cn.anoxia.springframework.aop.MethodBeforeAdvice;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * The class MethodBeforeAdviceInterceptor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:51
 */
public class MethodBeforeAdviceInterceptor implements MethodInterceptor {

  private MethodBeforeAdvice advice;

  public MethodBeforeAdviceInterceptor(){}

  public MethodBeforeAdviceInterceptor(MethodBeforeAdvice advice){
    this.advice = advice;
  }

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    this.advice.before(invocation.getMethod(),invocation.getArguments(),invocation.getThis());
    return invocation.proceed();
  }

  public MethodBeforeAdvice getAdvice() {
    return advice;
  }

  public void setAdvice(MethodBeforeAdvice advice) {
    this.advice = advice;
  }
}
