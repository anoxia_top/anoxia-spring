/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.core.io;

import org.springframework.util.Assert;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author huangle
 * 默认资源加载器
 */
public class DefaultResourceLoader implements ResourceLoader{
  @Override
  public Resource getResource(String location) {
    Assert.notNull(location,"Location must not be null");
    if (location.startsWith(CLASSPATH_URL_PREFIX)){
      return new ClassPathResource(location.substring(CLASSPATH_URL_PREFIX.length()));
    }else {
      try {
        URL url = new URL(location);
        return new UrlResource(url);
      }catch (MalformedURLException e){
        return new FileSystemResource(location);
      }
    }
  }
}
