/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.core.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author huangle
 */
public class FileSystemResource implements Resource{

  private final File file;

  private final String path;

  public FileSystemResource(File file){
    this.file = file;
    this.path = file.getPath();
  }

  public FileSystemResource(String path){
    this.file = new File(path);
    this.path = path;
  }

  @Override
  public InputStream getInputStream() throws IOException {
    return new FileInputStream(this.file);
  }

  public final String getPath(){
    return this.path;
  }
}
