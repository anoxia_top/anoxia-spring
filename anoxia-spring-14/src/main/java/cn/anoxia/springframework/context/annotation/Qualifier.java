package cn.anoxia.springframework.context.annotation;

import java.lang.annotation.*;

/**
 * The class Qualifier.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 18:57
 */
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER,ElementType.TYPE,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Qualifier {

  String value() default "";

}
