package cn.anoxia.springframework.context.event;

import java.util.Date;

/**
 * The class ContextClosedEvent.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:23
 */
public class ContextClosedEvent extends ApplicationContextEvent {
  /**
   * Constructs a prototypical Event.
   *
   * @param source The object on which the Event initially occurred.
   * @throws IllegalArgumentException if source is null.
   */
  public ContextClosedEvent(Object source) {
    super(source);
    System.out.println("发布ContextClosedEvent事件："+new Date());
  }
}
