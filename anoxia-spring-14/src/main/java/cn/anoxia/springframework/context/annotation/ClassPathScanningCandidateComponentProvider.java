package cn.anoxia.springframework.context.annotation;

import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.utils.ClassUtils;
import cn.hutool.core.util.ClassUtil;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * The class ClassPathScanningCandidateComponentProvider.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 14:49
 */
public class ClassPathScanningCandidateComponentProvider {

  /**
   * @Description: 扫描添加注解的类，用于初始化beandefinition
   * @author huangle
   * @Date 2021/8/31-14:52
   * @param basePackage
   * @return: java.util.Set<cn.anoxia.springframework.beans.factory.BeanDefinition>
   */
  public Set<BeanDefinition> findCandidateComponents(String basePackage){
    Set<BeanDefinition> candidates = new LinkedHashSet<>();
    Set<Class<?>> classes = ClassUtil.scanPackageByAnnotation(basePackage, Component.class);
    for (Class<?> clazz : classes){
      candidates.add(new BeanDefinition(clazz));
    }
    return candidates;
  }

}
