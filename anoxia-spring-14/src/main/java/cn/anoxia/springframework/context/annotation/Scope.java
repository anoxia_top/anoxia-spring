package cn.anoxia.springframework.context.annotation;

import java.lang.annotation.*;

/**
 * The class Scope.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 14:46
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Scope {

  String value() default "singleton";

}
