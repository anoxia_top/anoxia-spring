/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.service;

/**
 * @author huangle
 */
public class UserService {

  private String name;

  public UserService(String name){
    this.name = name;
  }

  public void getUser(){
    System.out.println("02 获取用户信息"+this.name);
  }

  @Override
  public String toString() {
    return "UserService{" +
            "name='" + name + '\'' +
            '}';
  }
}
