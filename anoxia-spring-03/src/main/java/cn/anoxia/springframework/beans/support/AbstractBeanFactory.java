/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.beans.BeanFactory;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.factory.BeanDefinition;

/**
 * @author huangle
 */
public abstract class AbstractBeanFactory extends DefaultSingletonBeanRegistry implements BeanFactory {

  @Override
  public Object getBean(String name) throws BeansException {
    Object singleton = getSingleton(name);
    if (singleton != null){
      return singleton;
    }
    BeanDefinition definition = getBeanDefinition(name);
//    return createBean(name,definition);
    return null;
  }

  @Override
  public Object getBean(String name,Object... args) throws BeansException {
    Object singleton = getSingleton(name);
    if (singleton != null){
      return singleton;
    }
    BeanDefinition definition = getBeanDefinition(name);
    return createBean(name,definition,args);
  }



  /**
   * 获取beanDefinition
   * @param name
   * @return {@link BeanDefinition}
   * @throws
   * @author huangle
   * @date 2021/8/23
   */
  protected abstract BeanDefinition getBeanDefinition(String name) throws BeansException;

  protected abstract Object createBean(String name, BeanDefinition beanDefinition,Object[] args) throws BeansException;
}
