package cn.anoxia.springframework.beans;/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */

import java.util.Map;

/**
 * @author huangle
 */
public interface ListableBeanFactory extends BeanFactory{

  /**
   * 按照类型返回 Bean 实例
   * @param type
   * @param <T>
   * @return
   * @throws
   */
  <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException;

  /**
   * Return the names of all beans defined in this registry.
   *
   * 返回注册表中所有的Bean名称
   */
  String[] getBeanDefinitionNames();



}
