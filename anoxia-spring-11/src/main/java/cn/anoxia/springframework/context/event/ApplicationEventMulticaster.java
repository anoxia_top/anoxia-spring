package cn.anoxia.springframework.context.event;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.context.ApplicationEvent;
import cn.anoxia.springframework.context.ApplicationListener;

/**
 * The class ApplicationEventMulticaster.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:24
 */
public interface ApplicationEventMulticaster {

  /**
   * Add a listener to be notified of all events.
   * @param listener the listener to add
   */
  void addApplicationListener(ApplicationListener<?> listener);

  /**
   * Remove a listener from the notification list.
   * @param listener the listener to remove
   */
  void removeApplicationListener(ApplicationListener<?> listener);

  /**
   * Multicast the given application event to appropriate listeners.
   * @param event the event to multicast
   */
  void multicastEvent(ApplicationEvent event) throws BeansException;

}
