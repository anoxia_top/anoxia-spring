package cn.anoxia.springframework.context;

import java.util.EventListener;

/**
 * The class ApplicationListener.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:34
 */
public interface ApplicationListener<E extends ApplicationEvent> extends EventListener {

  void onApplicationEvent(E event);

}
