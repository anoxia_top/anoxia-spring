package cn.anoxia.springframework.aop;

import org.aopalliance.intercept.MethodInterceptor;

/**
 * The class AdvisedSupport.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 17:56
 */
public class AdvisedSupport {

  // 被代理的对象
  private TargetSource targetSource;

  // 方法拦截器
  private MethodInterceptor methodInterceptor;

  // 方法匹配器
  private MethodMatcher methodMatcher;

  public TargetSource getTargetSource() {
    return targetSource;
  }

  public void setTargetSource(TargetSource targetSource) {
    this.targetSource = targetSource;
  }

  public MethodInterceptor getMethodInterceptor() {
    return methodInterceptor;
  }

  public void setMethodInterceptor(MethodInterceptor methodInterceptor) {
    this.methodInterceptor = methodInterceptor;
  }

  public MethodMatcher getMethodMatcher() {
    return methodMatcher;
  }

  public void setMethodMatcher(MethodMatcher methodMatcher) {
    this.methodMatcher = methodMatcher;
  }
}
