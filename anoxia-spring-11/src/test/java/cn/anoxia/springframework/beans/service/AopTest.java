package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.aop.AdvisedSupport;
import cn.anoxia.springframework.aop.MethodMatcher;
import cn.anoxia.springframework.aop.TargetSource;
import cn.anoxia.springframework.aop.aspectj.AspectJExpressionPointcut;
import cn.anoxia.springframework.aop.framework.Cglib2AopProxy;
import cn.anoxia.springframework.aop.framework.JdkDynamicAopProxy;
import org.junit.jupiter.api.Test;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * The class AopTest.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 17:56
 */
public class AopTest {

  @Test
  public void test_aop() throws NoSuchMethodException {
    AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut("execution(* cn.anoxia.springframework.beans.service.UserService.*())");
    Class<UserService> clazz = UserService.class;
    Method name = clazz.getDeclaredMethod("queryUserInfo");
    System.out.println(pointcut.matches(clazz));
    System.out.println(pointcut.matches(name,clazz));
  }


  @Test
  public void test_proxy_method(){

    IUserService userService = new UserService();
    AdvisedSupport advisedSupport = new AdvisedSupport();
    advisedSupport.setTargetSource(new TargetSource(userService));
    advisedSupport.setMethodInterceptor(new UserServiceInterceptor());
    advisedSupport.setMethodMatcher(new AspectJExpressionPointcut("execution(* cn.anoxia.springframework.beans.service.IUserService.*(..))"));

    IUserService proxy = (IUserService) new JdkDynamicAopProxy(advisedSupport).getProxy();

    System.out.println("测试调用queryUserInfo："+proxy.queryUserInfo());

    IUserService cglib_proxy = (IUserService) new Cglib2AopProxy(advisedSupport).getProxy();

    System.out.println("测试cglib："+cglib_proxy.register("anoxia"));
  }

}
