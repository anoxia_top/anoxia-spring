package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.support.DefaultListableBeanFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
class UserServiceTest {

  @Test
  public void test() throws BeansException {
    // 初始化spring工厂
    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
    // 初始化一个beanDefinition,里面有bean的基本信息
    BeanDefinition beanDefinition = new BeanDefinition(UserService.class);

    // 把bean加入到工厂中
    beanFactory.registerBeanDefinition("userService",beanDefinition);

    UserService userService = (UserService) beanFactory.getBean("userService");
    userService.getUser();

    UserService userService1 = (UserService) beanFactory.getBean("userService");
    userService1.getUser();

    System.out.println(userService==userService1);
  }

}