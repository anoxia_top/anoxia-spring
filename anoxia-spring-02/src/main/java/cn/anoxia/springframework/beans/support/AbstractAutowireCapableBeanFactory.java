/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.factory.BeanDefinition;

/**
 * @author huangle
 * 所有bean的初始化类
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory{

  @Override
  protected Object createBean(String name, BeanDefinition beanDefinition) throws BeansException {

    Object bean = null;
    try {
      bean = beanDefinition.getBeanClass().newInstance();
    }catch (InstantiationException | IllegalAccessException e){
      throw new BeansException("创建bean失败，异常信息",e);
    }

    addSingleton(name,bean);
    return bean;

  }
}
