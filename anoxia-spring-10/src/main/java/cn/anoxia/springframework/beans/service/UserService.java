/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeanFactory;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.dao.IUserDao;
import cn.anoxia.springframework.beans.dao.UserDao;
import cn.anoxia.springframework.beans.factory.*;
import cn.anoxia.springframework.context.ApplicationContext;
import lombok.Data;

import java.lang.reflect.InvocationTargetException;

/**
 * @author huangle
 */
public class UserService implements BeanNameAware, BeanClassLoaderAware, BeanFactoryAware,ApplicationContextAware {

  private ApplicationContext context;
  private BeanFactory beanFactory;

  private String id;

  private IUserDao userDao;

  private String company;

  private String phone;

  public String queryUserName(){
    return "02 获取用户信息："+userDao.queryUserName(id);
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.context = applicationContext;
  }

  @Override
  public void setBeanClassLoader(ClassLoader classLoader) {
    System.out.println("类加载器："+classLoader);
  }

  @Override
  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = beanFactory;
  }

  @Override
  public void setBeanName(String name) {
    System.out.println("类名称："+name);
  }

  public ApplicationContext getContext() {
    return context;
  }

  public void setContext(ApplicationContext context) {
    this.context = context;
  }

  public BeanFactory getBeanFactory() {
    return beanFactory;
  }

  public IUserDao getUserDao() {
    return userDao;
  }

  public void setUserDao(IUserDao userDao) {
    this.userDao = userDao;
  }
}
