package cn.anoxia.springframework.context.event;

import java.util.Date;

/**
 * The class ContextRefreshedEvent.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:23
 */
public class ContextRefreshedEvent extends ApplicationContextEvent {
  /**
   * Constructs a prototypical Event.
   *
   * @param source The object on which the Event initially occurred.
   * @throws IllegalArgumentException if source is null.
   */
  public ContextRefreshedEvent(Object source) {
    super(source);
    System.out.println("发布ContextRefreshedEvent事件:"+new Date());
  }
}
