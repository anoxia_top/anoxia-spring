package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.PropertyValues;
import cn.anoxia.springframework.beans.dao.UserDao;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.factory.BeanReference;
import cn.anoxia.springframework.beans.factory.config.BeanPostProcessor;
import cn.anoxia.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import cn.anoxia.springframework.beans.service.common.MyBeanFactoryPostProcessor;
import cn.anoxia.springframework.beans.service.common.MyBeanPostProcessor;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import cn.anoxia.springframework.beans.support.DefaultListableBeanFactory;
import cn.anoxia.springframework.core.io.DefaultResourceLoader;
import cn.anoxia.springframework.core.io.Resource;
import cn.hutool.core.io.IoUtil;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openjdk.jol.info.ClassLayout;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
class UserServiceTest {

  @Test
  public void test_prototype() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    System.out.println(applicationContext.getBean("proxyUserDao").toString());

    // 2. 获取Bean对象调用方法
    UserService userService1 = applicationContext.getBean("userService", UserService.class);
    UserService userService2 = applicationContext.getBean("userService", UserService.class);

    System.out.println(userService1);
    System.out.println(userService2);

    System.out.println(userService1+"十六进制哈希："+Integer.toHexString(Integer.parseInt(String.valueOf(userService1.hashCode()))));
    System.out.println(ClassLayout.parseInstance(userService1).toPrintable());
  }

  @Test
  public void test_proxy() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    UserService userService = applicationContext.getBean("userService", UserService.class);
    System.out.println(userService.queryUserName());
  }



}