package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.service.event.CustomEvent;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import org.junit.jupiter.api.Test;

/**
 * The class EventTest.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 11:30
 */
public class EventTest {

  @Test
  public void test_event() throws BeansException {
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    applicationContext.publishEvent(new CustomEvent(applicationContext.getClass().getName(),5734524L,"hello"));
    applicationContext.registerShutdownHook();
  }

}
