/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.service.common;

import cn.anoxia.springframework.beans.factory.config.BeanPostProcessor;
import cn.anoxia.springframework.beans.service.UserService;
import cn.hutool.core.util.HexUtil;
import org.springframework.beans.BeansException;

public class MyBeanPostProcessor implements BeanPostProcessor {

  /**
   * 初始化bean的前置处理
   *
   * @param bean
   * @param beanName
   * @return {@link Object}
   * @throws
   * @author huangle
   * @date 2021/8/26
   */
  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
      if ("userService".equals(beanName)) {
        System.out.println("postpress前置处理，为userService设置company属性");
        UserService userService = (UserService) bean;
        System.out.println("前置处理时userService对象："+userService.toString());
        String s = HexUtil.encodeHexStr(userService.getPhone());
        userService.setPhone(s);
        userService.setCompany("改为：北京");
      }
      return bean;
  }

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    System.out.println("postprocess后置处理");
    return bean;
  }

}
