/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * @author huangle
 */
public class PropertyValues {

  private final List<PropertyValue> propertyValueList = new ArrayList<>();

  public void addPropertyValue(PropertyValue pv){
    this.propertyValueList.add(pv);
  }

  public PropertyValue[] getPropertyValues(){
    return this.propertyValueList.toArray(new PropertyValue[0]);
  }

  public PropertyValue getPropertyValue(String name){
    return this.propertyValueList.stream()
            .filter(pv ->pv.getName().equals(name))
            .findFirst()
            .orElse(null);
  }

}
