/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.PropertyValues;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.factory.BeanReference;
import cn.hutool.core.bean.BeanUtil;


import java.lang.reflect.Constructor;

/**
 * @author huangle
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory{

  private InstantiationStrategy instantiationStrategy = new CglibSubclassingInstantiationStrategy();

  @Override
  protected Object createBean(String name, BeanDefinition beanDefinition, Object[] args) throws BeansException {

    Object bean = null;
    try {
      bean = createBeanInstance(beanDefinition,name,args);
      // 注入属性
      applyPropertyValues(name,bean,beanDefinition);
    }catch (Exception e){
      throw new BeansException("创建bean失败，异常信息",e);
    }
    // 添加单例到工厂
    addSingleton(name,bean);
    return bean;

  }

  protected Object createBeanInstance(BeanDefinition beanDefinition, String beanName, Object[] args) throws BeansException {
    Constructor constructor = null;
    Class<?> beanClass = beanDefinition.getBeanClass();
    Constructor<?>[] declaredConstructors = beanClass.getDeclaredConstructors();
    for (Constructor ctor : declaredConstructors) {
      if (args != null && ctor.getParameterTypes().length == args.length){
        constructor = ctor;
        break;
      }
    }
    return getInstantiationStrategy().instantiate(beanDefinition,beanName,constructor,args);
  }

  protected void applyPropertyValues(String beanName,Object bean,BeanDefinition beanDefinition){
    try {
      PropertyValues propertyValues = beanDefinition.getPropertyValues();
      if (propertyValues == null){
        return;
      }
      for (PropertyValue propertyValue : propertyValues.getPropertyValues()) {
        String name = propertyValue.getName();
        Object value = propertyValue.getValue();
        // 注入的为引用对象
        if (value instanceof BeanReference){
          BeanReference beanReference = (BeanReference) value;
          value = getBean(beanReference.getBeanName());
        }
        BeanUtil.setFieldValue(bean, name, value);
      }
    } catch (BeansException e) {
      System.out.println("注入属性值失败："+beanName);
    }
  }

  public InstantiationStrategy getInstantiationStrategy() {
    return instantiationStrategy;
  }

  public void setInstantiationStrategy(InstantiationStrategy instantiationStrategy) {
    this.instantiationStrategy = instantiationStrategy;
  }
}
