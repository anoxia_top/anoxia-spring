/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.dao;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huangle
 */
public class UserDao {

  private static Map<String,String> map = new HashMap<>();

  static {
    map.put("001","Anoxia");
    map.put("002","xiaoming");
  }

  public String queryUserName(String id){
    return map.get(id);
  }

}
