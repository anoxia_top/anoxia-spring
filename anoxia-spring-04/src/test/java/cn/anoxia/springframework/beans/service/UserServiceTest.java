package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.PropertyValues;
import cn.anoxia.springframework.beans.dao.UserDao;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.factory.BeanReference;
import cn.anoxia.springframework.beans.support.DefaultListableBeanFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
class UserServiceTest {

  @Test
  public void test() throws BeansException {
    // 初始化bean工厂
    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

    // 注册bean
    beanFactory.registerBeanDefinition("userDao",new BeanDefinition(UserDao.class));

    // 注入属性
    PropertyValues propertyValues = new PropertyValues();
    propertyValues.addPropertyValue(new PropertyValue("id","001"));
    propertyValues.addPropertyValue(new PropertyValue("userDao",new BeanReference("userDao")));

    BeanDefinition definition = new BeanDefinition(UserService.class, propertyValues);
    beanFactory.registerBeanDefinition("userService",definition);

    // 使用
    UserService bean = (UserService) beanFactory.getBean("userService");
    bean.getUser();
  }

}