/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.core.io.DefaultResourceLoader;
import cn.anoxia.springframework.core.io.ResourceLoader;

/**
 * @author huangle
 */
public abstract class AbstractBeanDefinitionReader implements BeanDefinitionReader{

  private final BeanDefinitionRegistry registry;

  private ResourceLoader resourceLoader;

  public AbstractBeanDefinitionReader(BeanDefinitionRegistry registry){
    this(registry,new DefaultResourceLoader());
  }

  public AbstractBeanDefinitionReader(BeanDefinitionRegistry registry,ResourceLoader resourceLoader){
    this.registry = registry;
    this.resourceLoader = resourceLoader;
  }


  @Override
  public BeanDefinitionRegistry getRegistry() {
    return registry;
  }

  @Override
  public ResourceLoader getResourceLoader() {
    return resourceLoader;
  }
}
