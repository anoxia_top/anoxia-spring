/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huangle
 */
public class DefaultSingletonBeanRegistry implements SingletonBeanRegistry{

  protected static final Object NULL_OBJECT = new Object();

  private Map<String,Object> singletonObjects = new HashMap<>();


  @Override
  public Object getSingleton(String name) {
    return singletonObjects.get(name);
  }

  protected void addSingleton(String beanName, Object singletonObject){
    singletonObjects.put(beanName, singletonObject);
  }
}
