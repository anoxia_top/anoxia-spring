package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.Aware;
import cn.anoxia.springframework.beans.BeanFactory;
import cn.anoxia.springframework.beans.BeansException;

/**
 * The class BeanFactoryAware.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 14:36
 */
public interface BeanFactoryAware extends Aware {
  void setBeanFactory(BeanFactory beanFactory) throws BeansException;
}
