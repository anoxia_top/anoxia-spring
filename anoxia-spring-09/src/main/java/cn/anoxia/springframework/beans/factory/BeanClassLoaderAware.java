package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.Aware;

/**
 * The class BeanClassLoaderAware.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 14:39
 */
public interface BeanClassLoaderAware extends Aware {

  void setBeanClassLoader(ClassLoader classLoader);

}
