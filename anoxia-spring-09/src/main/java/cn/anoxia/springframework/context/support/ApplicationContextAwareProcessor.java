package cn.anoxia.springframework.context.support;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.factory.ApplicationContextAware;
import cn.anoxia.springframework.beans.factory.config.BeanPostProcessor;
import cn.anoxia.springframework.context.ApplicationContext;

/**
 * The class ApplicationContextAwareProcessor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 14:43
 */
public class ApplicationContextAwareProcessor implements BeanPostProcessor {

  private final ApplicationContext context;

  public ApplicationContextAwareProcessor(ApplicationContext context){
    this.context = context;
  }

  @Override
  public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
    if (bean instanceof ApplicationContextAware){
      ((ApplicationContextAware) bean).setApplicationContext(context);
    }
    return bean;
  }

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    return bean;
  }
}
