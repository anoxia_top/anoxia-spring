/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.service.common;


import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.PropertyValues;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.factory.config.BeanFactoryPostProcessor;
import cn.anoxia.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import cn.hutool.core.util.HexUtil;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

  /**
   * 修改beandefinition的信息
   *
   * @param beanFactory
   * @return
   * @throws
   * @author huangle
   * @date 2021/8/26
   */
  @Override
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
    BeanDefinition beanDefinition = beanFactory.getBeanDefinition("userService");
    // 获得类的修饰信息
    // 修改元素
    PropertyValues propertyValues = beanDefinition.getPropertyValues();
    propertyValues.addPropertyValue(new PropertyValue("company", "改为：字节跳动"));
    propertyValues.addPropertyValue(new PropertyValue("phone", HexUtil.encodeHexStr("19173569211")));
  }
}
