/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.dao.UserDao;

/**
 * @author huangle
 */
public class UserService {

  private String id;

  private UserDao userDao;

  private String company;

  public String getUser(){
    System.out.println("02 获取用户信息："+userDao.queryUserName(id));
    System.out.println(this.toString());
    return "02 获取用户信息："+userDao.queryUserName(id);
  }

  @Override
  public String toString() {
    return "UserService{" +
            "id='" + id + '\'' +
            ", userDao=" + userDao +
            ", company='" + company + '\'' +
            '}';
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public UserDao getUserDao() {
    return userDao;
  }

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }
}
