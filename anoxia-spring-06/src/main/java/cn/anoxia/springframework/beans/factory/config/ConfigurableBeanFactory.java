package cn.anoxia.springframework.beans.factory.config;

import cn.anoxia.springframework.beans.BeanFactory;
import cn.anoxia.springframework.beans.support.SingletonBeanRegistry;

/**
 * The class ConfigurableBeanFactory.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/26 17:44
 * ${tags}
 * @version: ${Revision} ${date} ${LastChangedBy}
 */
public interface ConfigurableBeanFactory extends SingletonBeanRegistry,BeanFactory {

  void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);

}
