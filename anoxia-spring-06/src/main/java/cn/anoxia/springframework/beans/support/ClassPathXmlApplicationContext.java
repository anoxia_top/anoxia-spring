/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.beans.BeansException;

import java.util.Map;

public class ClassPathXmlApplicationContext extends AbstractXmlApplicationContext{

  private String[] configLocations;

  public ClassPathXmlApplicationContext() {
  }

  /**
   * 从 XML 中加载 BeanDefinition，并刷新上下文
   *
   * @param configLocations
   * @throws BeansException
   */
  public ClassPathXmlApplicationContext(String configLocations) throws BeansException {
    this(new String[]{configLocations});
  }

  /**
   * 从 XML 中加载 BeanDefinition，并刷新上下文
   * @param configLocations
   * @throws BeansException
   */
  public ClassPathXmlApplicationContext(String[] configLocations) throws BeansException {
    this.configLocations = configLocations;
    refresh();
  }

  @Override
  protected String[] getConfigLocations() {
    return configLocations;
  }

}
