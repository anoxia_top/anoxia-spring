# spring源码

## 1、AnnotationConfigApplicationContext 类

> 提供给用户的入口类，通过该类初始化spring容器

```java
public AnnotationConfigApplicationContext(Class<?>... annotatedClasses) {
	this();
    // 注册注解类
	register(annotatedClasses);
	// 注册初始化spring容器	
    refresh();
}
```



### 初始化方法 refresh

```java
@Override
	public void refresh() throws BeansException, IllegalStateException {
		synchronized (this.startupShutdownMonitor) {
			// Prepare this context for refreshing.
            // 刷新上下文
			prepareRefresh();

			// Tell the subclass to refresh the internal bean factory.
            // 获取bean工厂
			ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

			// Prepare the bean factory for use in this context.
            // 准备bean工厂
			prepareBeanFactory(beanFactory);

			try {
				// Allows post-processing of the bean factory in context subclasses.
                // bean初始化之前的的设置（在bean初始化之前，可以修改beandifinition的内容）
				postProcessBeanFactory(beanFactory);

				// Invoke factory processors registered as beans in the context.
                // 	在 Bean 实例化之前，执行 BeanFactoryPostProcessor 
				invokeBeanFactoryPostProcessors(beanFactory);

				// Register bean processors that intercept bean creation.
                //BeanPostProcessor 需要提前于其他 Bean 对象实例化之前执行注册操作
				registerBeanPostProcessors(beanFactory);

				// Initialize message source for this context.
                // 初始化加载上下文资源方式
				initMessageSource();

				// Initialize event multicaster for this context.
                // 初始化上下文事件通知多播器
				initApplicationEventMulticaster();

				// Initialize other special beans in specific context subclasses.
                // 初始化特定上下文子类中的其他特殊bean。
				onRefresh();

				// Check for listener beans and register them.
                // 初始化监听事件
				registerListeners();

				// Instantiate all remaining (non-lazy-init) singletons.
                // 完成bean工厂的初始化
				finishBeanFactoryInitialization(beanFactory);

				// Last step: publish corresponding event.
                // 完成上下文加载
				finishRefresh();
			}

			catch (BeansException ex) {
				if (logger.isWarnEnabled()) {
					logger.warn("Exception encountered during context initialization - " +
							"cancelling refresh attempt: " + ex);
				}

				// Destroy already created singletons to avoid dangling resources.
                // 销毁bean
				destroyBeans();

				// Reset 'active' flag.
                // 清除上线文
				cancelRefresh(ex);

				// Propagate exception to caller.
				throw ex;
			}

			finally {
				// Reset common introspection caches in Spring's core, since we
				// might not ever need metadata for singleton beans anymore...
                // 重置缓存
				resetCommonCaches();
			}
		}
	}
```

