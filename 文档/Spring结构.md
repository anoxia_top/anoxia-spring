# Spring核心结构

## 1、BeanFactory

> beanFactory spring的核心容器，所有的bean会被添加并在该容器中维护。

### 1、AbstractBeanFactory

![image-20210913161943749](E:/anoxia/anoxia-spring/%E6%96%87%E6%A1%A3/img/image-20210913161943749.png)

> 完成bean的获取，注册，获取beanDefinition信息

### 2、DefaultListableBeanFactory

> 完成beanDefinition的获取与注册

![image-20210913162116761](E:/anoxia/anoxia-spring/%E6%96%87%E6%A1%A3/img/image-20210913162116761.png)

### 3、DefaultSingletonBeanRegistry

> 默认的单例bean注册类

​    ![image-20210913162346134](E:/anoxia/anoxia-spring/%E6%96%87%E6%A1%A3/img/image-20210913162346134-1631521428639.png)

### 4、AbstractAutowireCapableBeanFactory

> 默认创建bean的类

<img src="file:///E:/anoxia/anoxia-spring/%E6%96%87%E6%A1%A3/img/image-20210913162541692.png" title="" alt="image-20210913162541692" data-align="inline">

## 2、BeanDefinition

> 保存类的基本参数属性、名称、单例原型模式配置信息

详细参数信息

![](E:\anoxia\anoxia-spring\文档\img\BeanDefinition.png)

## 3、PropertyValues

保存参数对应信息（）

## 4、Resource

## 5、PostProcess

## 6、Aware

## 7、FactoryBean

## 8、Event


