package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.PropertyValues;
import cn.anoxia.springframework.beans.dao.UserDao;
import cn.anoxia.springframework.beans.factory.BeanDefinition;
import cn.anoxia.springframework.beans.factory.BeanReference;
import cn.anoxia.springframework.beans.factory.config.BeanPostProcessor;
import cn.anoxia.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import cn.anoxia.springframework.beans.service.common.MyBeanFactoryPostProcessor;
import cn.anoxia.springframework.beans.service.common.MyBeanPostProcessor;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import cn.anoxia.springframework.beans.support.DefaultListableBeanFactory;
import cn.anoxia.springframework.core.io.DefaultResourceLoader;
import cn.anoxia.springframework.core.io.Resource;
import cn.hutool.core.io.IoUtil;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
class UserServiceTest {

  @Test
  public void test() throws BeansException {
    // 初始化bean工厂
    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

    // 注册bean
    beanFactory.registerBeanDefinition("userDao",new BeanDefinition(UserDao.class));

    // 注入属性
    PropertyValues propertyValues = new PropertyValues();
    propertyValues.addPropertyValue(new PropertyValue("id","001"));
    propertyValues.addPropertyValue(new PropertyValue("userDao",new BeanReference("userDao")));

    BeanDefinition definition = new BeanDefinition(UserService.class, propertyValues);
    beanFactory.registerBeanDefinition("userService",definition);

    // 使用
    UserService bean = (UserService) beanFactory.getBean("userService");
    bean.getUser();
  }

  private DefaultResourceLoader resourceLoader = new DefaultResourceLoader(); ;


  @Before
  public void init(){
    resourceLoader = new DefaultResourceLoader();
  }

  @Test
  public void test_classpath() throws Exception{
    Resource resource = resourceLoader.getResource("classpath:important.properties");
    InputStream inputStream = resource.getInputStream();
    String s = IoUtil.read(inputStream,"UTF-8");
    System.out.println(s);
  }

  @Test
  public void test_file() throws IOException {
    Resource resource = resourceLoader.getResource("src/test/resources/important.properties");
    InputStream inputStream = resource.getInputStream();
    String content = IoUtil.read(inputStream,"utf-8");
    System.out.println(content);
  }

  @Test
  public void test_url() throws Exception{
    Resource resource = resourceLoader.getResource("https://github.com/fuzhengwei/small-spring/important.properties");
    InputStream inputStream = resource.getInputStream();
    String content = IoUtil.read(inputStream,"utf-8");
    System.out.println(content);
  }

  @Test
  public void test_xml() throws BeansException {
    // 1.初始化 BeanFactory
    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

    // 2. 读取配置文件&注册Bean
    XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
    reader.loadBeanDefinitions("classpath:spring.xml");

    // 3. 获取Bean对象调用方法
    UserService userService = beanFactory.getBean("userService",UserService.class);
    String result = userService.getUser();
    System.out.println("测试结果：" + result);
  }


  @Test
  public void test_xml1() throws BeansException {
    // 1.初始化 BeanFactory
//    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");


    DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();

    // 2. 读取配置文件&注册Bean
    XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
    reader.loadBeanDefinitions("classpath:spring.xml");

    // 3. BeanDefinition 加载完成 & Bean实例化之前，修改 BeanDefinition 的属性值
    MyBeanFactoryPostProcessor beanFactoryPostProcessor = new MyBeanFactoryPostProcessor();
    beanFactoryPostProcessor.postProcessBeanFactory(beanFactory);

    // 4. Bean实例化之后，修改 Bean 属性信息
    MyBeanPostProcessor beanPostProcessor = new MyBeanPostProcessor();
    beanFactory.addBeanPostProcessor(beanPostProcessor);

    // 2. 获取Bean对象调用方法
    UserService userService = beanFactory.getBean("userService", UserService.class);
    String result = userService.getUser();
    System.out.println("测试结果：" + result);
  }


  @Test
  public void test_xml2() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 2. 获取Bean对象调用方法
    UserService userService = applicationContext.getBean("userService", UserService.class);
    String result = userService.getUser();
    System.out.println("测试结果：" + result);
  }


  @Test
  public void test_xml3() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    // 2. 获取Bean对象调用方法
    UserService userService = applicationContext.getBean("userService", UserService.class);
    String result = userService.getUser();
    System.out.println("测试结果：" + result);
  }



}