/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.support;

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.PropertyValue;
import cn.anoxia.springframework.beans.PropertyValues;
import cn.anoxia.springframework.beans.factory.*;
import cn.anoxia.springframework.beans.factory.config.BeanPostProcessor;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author huangle
 */
public abstract class AbstractAutowireCapableBeanFactory extends AbstractBeanFactory implements AutowireCapableBeanFactory {

  private InstantiationStrategy instantiationStrategy = new CglibSubclassingInstantiationStrategy();

  @Override
  protected Object createBean(String name, BeanDefinition beanDefinition, Object[] args) throws BeansException {

    Object bean = null;
    try {
      bean = createBeanInstance(beanDefinition,name,args);
      // 注入属性
      applyPropertyValues(name,bean,beanDefinition);
      // 执行 Bean 的初始化方法和 BeanPostProcessor 的前置和后置处理方法
      bean = initializeBean(name, bean, beanDefinition);
    }catch (Exception e){
      throw new BeansException("创建bean失败，异常信息",e);
    }
    // 注册销毁方法
    registerDisposableBeanIfNecessary(name,bean,beanDefinition);

    // 添加单例到工厂
    addSingleton(name,bean);
    return bean;

  }

  private Object initializeBean(String beanName, Object bean, BeanDefinition beanDefinition) throws BeansException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
    // 1. 执行 BeanPostProcessor Before 处理
    Object wrappedBean = applyBeanPostProcessorsBeforeInitialization(bean, beanName);

    // 待完成内容：invokeInitMethods(beanName, wrappedBean, beanDefinition);
    invokeInitMethods(beanName, wrappedBean, beanDefinition);

    // 2. 执行 BeanPostProcessor After 处理
    wrappedBean = applyBeanPostProcessorsAfterInitialization(bean, beanName);
    return wrappedBean;
  }

  private void invokeInitMethods(String beanName, Object bean, BeanDefinition beanDefinition) throws BeansException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    if (bean instanceof InitializingBean){
      ((InitializingBean) bean).afterPropertiesSet();
    }
    String initMethodName = beanDefinition.getInitMethodName();
    if (StrUtil.isNotEmpty(initMethodName)){
      Method method = beanDefinition.getBeanClass().getMethod(initMethodName);
      if (null == method){
        throw new BeansException("Could not find an init method named '" + initMethodName + "' on bean with name '" + beanName + "'");
      }
      method.invoke(bean);
    }

  }

  @Override
  public Object applyBeanPostProcessorsBeforeInitialization(Object existingBean, String beanName) throws BeansException {
    Object result = existingBean;
    for (BeanPostProcessor processor : getBeanPostProcessors()) {
      Object current = processor.postProcessBeforeInitialization(result, beanName);
      if (null == current) {
        return result;
      }
      result = current;
    }
    return result;
  }

  @Override
  public Object applyBeanPostProcessorsAfterInitialization(Object existingBean, String beanName) throws BeansException {
    Object result = existingBean;
    for (BeanPostProcessor processor : getBeanPostProcessors()) {
      Object current = processor.postProcessAfterInitialization(result, beanName);
      if (null == current) {
        return result;
      }
      result = current;
    }
    return result;
  }

  protected Object createBeanInstance(BeanDefinition beanDefinition, String beanName, Object[] args) throws BeansException {
    Constructor constructor = null;
    Class<?> beanClass = beanDefinition.getBeanClass();
    Constructor<?>[] declaredConstructors = beanClass.getDeclaredConstructors();
    for (Constructor ctor : declaredConstructors) {
      if (args != null && ctor.getParameterTypes().length == args.length){
        constructor = ctor;
        break;
      }
    }
    return getInstantiationStrategy().instantiate(beanDefinition,beanName,constructor,args);
  }

  protected void applyPropertyValues(String beanName,Object bean,BeanDefinition beanDefinition){
    try {
      PropertyValues propertyValues = beanDefinition.getPropertyValues();
      if (propertyValues == null){
        return;
      }
      for (PropertyValue propertyValue : propertyValues.getPropertyValues()) {
        String name = propertyValue.getName();
        Object value = propertyValue.getValue();
        // 注入的为引用对象
        if (value instanceof BeanReference){
          BeanReference beanReference = (BeanReference) value;
          value = getBean(beanReference.getBeanName());
        }
        BeanUtil.setFieldValue(bean, name, value);
      }
    } catch (BeansException e) {
      System.out.println("注入属性值失败："+beanName);
    }
  }

  public InstantiationStrategy getInstantiationStrategy() {
    return instantiationStrategy;
  }
  public void setInstantiationStrategy(InstantiationStrategy instantiationStrategy) {
    this.instantiationStrategy = instantiationStrategy;
  }

  protected void registerDisposableBeanIfNecessary(String beanName,Object bean,BeanDefinition beanDefinition){
    if (bean instanceof DisposableBean || StrUtil.isNotEmpty(beanDefinition.getDestroyMethodName())){
      registerDisposableBean(beanName, new DisposableBeanAdapter(bean, beanName, beanDefinition));
    }
  }

  protected abstract void registerDisposableBean(String beanName, DisposableBeanAdapter disposableBeanAdapter);
}
