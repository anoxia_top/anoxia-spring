/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */

import cn.anoxia.springframework.BeanDefinition;
import cn.anoxia.springframework.BeanFactory;
import cn.anoxia.springframework.service.UserService;
import org.junit.jupiter.api.Test;

public class UserServiceTest {


  @Test
  public void test1(){

//    初始化工厂
    BeanFactory factory = new BeanFactory();
    // 注册bean信息
    BeanDefinition definition = new BeanDefinition(new UserService());
    factory.registerBeanDefinition("userService",definition);
    // 获取bean信息
    UserService userService = (UserService) factory.getBean("userService");
    userService.getUser();
  }


}
