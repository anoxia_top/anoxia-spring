/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework;

import java.util.HashMap;
import java.util.Map;

/**
 * @author huangle
 * 代表了 Bean 对象的工厂，可以存放 Bean 定义到 Map 中以及获取
 */
public class BeanFactory {

  private Map<String,BeanDefinition> beanDefinitionMap = new HashMap<>();

  public Object getBean(String name) {
    return beanDefinitionMap.get(name).getBean();
  }

  public void registerBeanDefinition(String name,BeanDefinition beanDefinition){
    beanDefinitionMap.put(name,beanDefinition);
  }
}
