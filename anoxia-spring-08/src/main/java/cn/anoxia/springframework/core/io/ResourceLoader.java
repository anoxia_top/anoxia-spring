package cn.anoxia.springframework.core.io;/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */

/**
 * @author huangle
 */
public interface ResourceLoader {

  String CLASSPATH_URL_PREFIX = "classpath:";

  Resource getResource(String location);

}
