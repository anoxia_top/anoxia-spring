package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.BeansException;

import java.lang.reflect.InvocationTargetException;

/**
 * The class DisposableBean.
 * <p>
 * Description: 销毁方法
 *
 * @author: huangle
 * @since: 2021/08/27 10:30
 * ${tags}
 * @version: ${Revision} ${date} ${LastChangedBy}
 */
public interface DisposableBean {

  /**
   * @Description: 所有自定义的销毁方法均实现该方法
   * @author huangle
   * @Date 2021/8/27-14:02
   * @param
   * @return: void
   */
  void destroy() throws BeansException, InvocationTargetException, IllegalAccessException, NoSuchMethodException;

}
