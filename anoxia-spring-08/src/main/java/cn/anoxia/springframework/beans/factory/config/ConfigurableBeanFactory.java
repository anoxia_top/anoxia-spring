package cn.anoxia.springframework.beans.factory.config;

import cn.anoxia.springframework.beans.BeanFactory;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.support.SingletonBeanRegistry;

/**
 * The class ConfigurableBeanFactory.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/26 17:44
 * ${tags}
 * @version: ${Revision} ${date} ${LastChangedBy}
 */
public interface ConfigurableBeanFactory extends SingletonBeanRegistry,BeanFactory {

  /**
   * @Description: 添加beanPostProcessor处理器
   * @author huangle
   * @Date 2021/8/27-14:00
   * @param beanPostProcessor
   * @return: void
   */
  void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);

  /**
   * @Description:  销毁对象
   * @author huangle
   * @Date 2021/8/27-14:00
   * @param
   * @return: void
   */
  void destroySingletons() throws BeansException;

}
