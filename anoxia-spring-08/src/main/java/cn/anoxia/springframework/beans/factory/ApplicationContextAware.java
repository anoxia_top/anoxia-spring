package cn.anoxia.springframework.beans.factory;

import cn.anoxia.springframework.beans.Aware;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.context.ApplicationContext;

/**
 * The class ApplicationContextAware.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 14:41
 */
public interface ApplicationContextAware extends Aware {

  void setApplicationContext(ApplicationContext applicationContext) throws BeansException;

}
