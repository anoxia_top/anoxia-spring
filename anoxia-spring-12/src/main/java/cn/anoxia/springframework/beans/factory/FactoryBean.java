package cn.anoxia.springframework.beans.factory;

/**
 * The class FactoryBean.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 16:39
 */
public interface FactoryBean<T> {

  T getObject() throws Exception;

  Class<?> getObjectType();

  boolean isSingleton();

}
