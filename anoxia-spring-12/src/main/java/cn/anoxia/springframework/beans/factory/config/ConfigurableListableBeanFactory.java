package cn.anoxia.springframework.beans.factory.config;
/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */

import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.ListableBeanFactory;
import cn.anoxia.springframework.beans.factory.AutowireCapableBeanFactory;
import cn.anoxia.springframework.beans.factory.BeanDefinition;

/**
 * @author huangle
 */
public interface ConfigurableListableBeanFactory extends
        ListableBeanFactory, AutowireCapableBeanFactory,ConfigurableBeanFactory {

  void preInstantiateSingletons() throws BeansException;

  BeanDefinition getBeanDefinition(String userService) throws BeansException;

  void addBeanPostProcessor(BeanPostProcessor beanPostProcessor);



}
