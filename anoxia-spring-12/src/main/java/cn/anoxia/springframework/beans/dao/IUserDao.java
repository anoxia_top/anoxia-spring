package cn.anoxia.springframework.beans.dao;

import cn.anoxia.springframework.beans.factory.FactoryBean;

/**
 * The class IUserDao.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/27 16:58
 */
public interface IUserDao {

  String queryUserName(String id);

}
