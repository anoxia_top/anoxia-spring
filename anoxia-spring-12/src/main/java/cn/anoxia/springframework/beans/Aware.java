package cn.anoxia.springframework.beans;

/**
 * The class Aware.
 * <p>
 * Description: 标记类接口，实现该接口可以被spring容器感知
 *
 * @author: huangle
 * @since: 2021/08/27 14:33
 */
public interface Aware {
}
