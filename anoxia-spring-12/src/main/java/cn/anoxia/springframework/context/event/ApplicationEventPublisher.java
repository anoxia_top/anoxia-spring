package cn.anoxia.springframework.context.event;

import cn.anoxia.springframework.context.ApplicationEvent;

/**
 * The class ApplicationEventPublisher.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:27
 */
public interface ApplicationEventPublisher {

  /**
   * @Description: 通知所有的监听者注册到application
   * @author huangle
   * @Date 2021/8/30-10:28
   * @param event 要发布的事件
   * @return: void
   */
  void publishEvent(ApplicationEvent event);

}
