package cn.anoxia.springframework.aop;

/**
 * The class PointcutAdvisor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:47
 */
public interface PointcutAdvisor extends Advisor {

  Pointcut getPointcut();

}
