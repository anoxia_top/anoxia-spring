package cn.anoxia.springframework.aop.aspectj;

import cn.anoxia.springframework.aop.Pointcut;
import cn.anoxia.springframework.aop.PointcutAdvisor;
import org.aopalliance.aop.Advice;

/**
 * The class AspectJExpressionPointcutAdvisor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:48
 */
public class AspectJExpressionPointcutAdvisor implements PointcutAdvisor {

  // 切面
  private AspectJExpressionPointcut pointcut;

  // 具体拦截的方法
  private Advice advice;

  // 表达式
  private String expression;

  public void setExpression(String expression) {
    this.expression = expression;
  }


  @Override
  public Pointcut getPointcut() {
    if (null == pointcut){
      pointcut = new AspectJExpressionPointcut(expression);
    }
    return pointcut;
  }

  @Override
  public Advice getAdvice() {
    return advice;
  }

  public void setAdvice(Advice advice) {
    this.advice = advice;
  }
}
