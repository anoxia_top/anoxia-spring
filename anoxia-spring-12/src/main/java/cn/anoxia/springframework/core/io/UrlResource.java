/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.core.io;

import org.springframework.util.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class UrlResource implements Resource{

  private final URL url;

  public UrlResource(URL url){
    Assert.notNull(url,"URL must not be null");
    this.url = url;
  }

  @Override
  public InputStream getInputStream() throws IOException {
    URLConnection connection = this.url.openConnection();
    try {
      return connection.getInputStream();
    }catch (IOException e){
      if (connection instanceof HttpURLConnection){
        ((HttpURLConnection) connection).disconnect();
      }
      throw e;
    }
  }
}
