package cn.anoxia.springframework.beans.service.event;

import cn.anoxia.springframework.context.ApplicationListener;
import cn.anoxia.springframework.context.event.ContextClosedEvent;

import java.util.Date;

/**
 * The class ContextClosedEventListener.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 13:56
 */
public class ContextClosedEventListener implements ApplicationListener<ContextClosedEvent> {
  @Override
  public void onApplicationEvent(ContextClosedEvent event) {
    System.out.println("closed监听者监听到关闭事件："+new Date());
  }
}
