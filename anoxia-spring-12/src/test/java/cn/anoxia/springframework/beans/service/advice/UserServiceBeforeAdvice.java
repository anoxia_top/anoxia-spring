package cn.anoxia.springframework.beans.service.advice;

import cn.anoxia.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

/**
 * The class UserServiceBeforeAdvice.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 11:13
 */
public class UserServiceBeforeAdvice implements MethodBeforeAdvice {
  @Override
  public void before(Method method, Object[] args, Object target) throws Throwable {
    System.out.println("前置处理：开启事务、校验参数");
    System.out.println("拦截方法："+method.getName());
    System.out.println("前置处理结束！");
  }
}
