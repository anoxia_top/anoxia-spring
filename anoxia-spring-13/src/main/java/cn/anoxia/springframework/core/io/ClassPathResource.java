/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.core.io;

import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author huangle
 */
public class ClassPathResource implements Resource{

  private final String path;

  private ClassLoader classLoader;

  public ClassPathResource(String path){
    this(path, null);
  }

  public ClassPathResource(String path,ClassLoader classLoader){
    Assert.notNull(path,"路径不能为空");
    this.path = path;
    this.classLoader = (classLoader != null ? classLoader : ClassUtils.getDefaultClassLoader());
  }

  @Override
  public InputStream getInputStream() throws IOException {
    InputStream stream = classLoader.getResourceAsStream(path);
    if (stream == null){
      throw new FileNotFoundException(
              this.path + " cannot be opened because it does not exist");
    }
    return stream;
  }
}
