package cn.anoxia.springframework.aop;

import java.lang.reflect.Method;

/**
 * The class MethodMatcher.
 * <p>
 * Description:
 * 方法匹配，找到表达式范围内匹配下的目标类和方法。
 *
 * @author: huangle
 * @since: 2021/08/31 9:21
 */
public interface MethodMatcher {


  boolean matches(Method method, Class<?> targetClass);

}
