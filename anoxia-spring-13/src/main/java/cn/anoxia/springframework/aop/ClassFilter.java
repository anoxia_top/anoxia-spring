package cn.anoxia.springframework.aop;

/**
 * The class ClassFilter.
 * <p>
 * Description:
 * 义类匹配类，用于切点找到给定的接口和目标类。
 * @author: huangle
 * @since: 2021/08/31 9:20
 */
public interface ClassFilter {

  boolean matches(Class<?> clazz);

}
