package cn.anoxia.springframework.aop;

import org.aopalliance.aop.Advice;
import org.springframework.aop.framework.Advised;

/**
 * The class BeforeAdvice.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:45
 */
public interface BeforeAdvice extends Advice {
}
