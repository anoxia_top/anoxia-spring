package cn.anoxia.springframework.aop;

import org.aopalliance.aop.Advice;

/**
 * The class Advisor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:47
 */
public interface Advisor {

  Advice getAdvice();

}
