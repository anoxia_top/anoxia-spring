package cn.anoxia.springframework.aop;

/**
 * The class Pointcut.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 9:20
 */
public interface Pointcut {

  /**
   *
   * Return the ClassFilter for this pointcut.
   * @return the ClassFilter (never <code>null</code>)
   */
  ClassFilter getClassFilter();

  /**
   * 方法匹配器
   * Return the MethodMatcher for this pointcut.
   * @return the MethodMatcher (never <code>null</code>)
   */
  MethodMatcher getMethodMatcher();

}
