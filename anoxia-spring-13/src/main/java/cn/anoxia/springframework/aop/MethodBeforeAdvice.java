package cn.anoxia.springframework.aop;

import java.lang.reflect.Method;

/**
 * The class MethodBeforeAdvice.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:46
 */
public interface MethodBeforeAdvice extends BeforeAdvice {

  void before(Method method,Object[] args, Object target) throws Throwable;


}
