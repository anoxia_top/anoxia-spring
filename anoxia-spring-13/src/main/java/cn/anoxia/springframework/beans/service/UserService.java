/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
package cn.anoxia.springframework.beans.service;

import cn.anoxia.springframework.beans.BeanFactory;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.dao.IUserDao;
import cn.anoxia.springframework.beans.dao.UserDao;
import cn.anoxia.springframework.beans.factory.*;
import cn.anoxia.springframework.context.ApplicationContext;
import cn.anoxia.springframework.context.annotation.Autowired;
import cn.anoxia.springframework.context.annotation.Component;
import cn.anoxia.springframework.context.annotation.Value;
import lombok.Data;

import java.lang.reflect.InvocationTargetException;
import java.util.Random;

/**
 * @author huangle
 */
@Component("userService")
public class UserService implements IUserService {

  @Value("${token}")
  private String token;

  @Autowired
  private UserDao userDao;

  @Override
  public String queryUserInfo() {
    try {
      Thread.sleep(new Random(1).nextInt(100));
    }catch (InterruptedException e){
      e.printStackTrace();
    }
    return userDao.queryUserName("001")+","+token;
  }

  @Override
  public String register(String name) {
    try {
      Thread.sleep(new Random(1).nextInt(100));
    }catch (InterruptedException e){
      e.printStackTrace();
    }
    return "注册用户："+name+"，注册成功！";
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public UserDao getUserDao() {
    return userDao;
  }

  public void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }
}
