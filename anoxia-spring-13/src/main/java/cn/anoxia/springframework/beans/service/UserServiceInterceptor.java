package cn.anoxia.springframework.beans.service;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * The class UserServiceInterceptor.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:12
 */
public class UserServiceInterceptor implements MethodInterceptor {

  @Override
  public Object invoke(MethodInvocation invocation) throws Throwable {
    long start = System.currentTimeMillis();
    try {
      return invocation.proceed();
    }finally {
      System.out.println("监控 - Begin By AOP");
      System.out.println("方法名称：" + invocation.getMethod());
      System.out.println("方法耗时：" + (System.currentTimeMillis() - start) + "ms");
      System.out.println("监控 - End\r\n");
    }
  }
}
