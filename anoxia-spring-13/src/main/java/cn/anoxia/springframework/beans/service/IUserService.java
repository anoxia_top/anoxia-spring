package cn.anoxia.springframework.beans.service;

/**
 * The class IUserService.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 10:07
 */
public interface IUserService {

  String queryUserInfo();

  String register(String name);

}
