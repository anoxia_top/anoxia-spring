package cn.anoxia.springframework.utils;

/**
 * The class StringValueResolver.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 15:54
 */
public interface StringValueResolver {
  String resolveStringValue(String strVal);
}
