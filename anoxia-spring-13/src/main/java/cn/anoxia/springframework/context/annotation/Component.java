package cn.anoxia.springframework.context.annotation;

import java.lang.annotation.*;

/**
 * The class Component.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 14:47
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Component {

  String value() default "";

}
