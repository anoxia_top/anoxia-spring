package cn.anoxia.springframework.context.event;

import cn.anoxia.springframework.context.ApplicationContext;
import cn.anoxia.springframework.context.ApplicationEvent;

/**
 * The class ApplicationContextEvent.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:21
 */
public class ApplicationContextEvent extends ApplicationEvent {
  /**
   * Constructs a prototypical Event.
   *
   * @param source The object on which the Event initially occurred.
   * @throws IllegalArgumentException if source is null.
   */
  public ApplicationContextEvent(Object source) {
    super(source);
  }

  /**
   * Get the <code>ApplicationContext</code> that the event was raised for.
   */
  public final ApplicationContext getApplicationContext(){
    return (ApplicationContext) getSource();
  }
}
