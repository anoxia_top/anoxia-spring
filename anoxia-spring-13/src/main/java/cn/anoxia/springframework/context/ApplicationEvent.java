package cn.anoxia.springframework.context;

import java.util.EventObject;

/**
 * The class ApplicationEvent.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 10:21
 */
public abstract class ApplicationEvent extends EventObject {
  /**
   * Constructs a prototypical Event.
   *
   * @param source The object on which the Event initially occurred.
   * @throws IllegalArgumentException if source is null.
   */
  public ApplicationEvent(Object source) {
    super(source);
  }
}
