package cn.anoxia.springframework.context.annotation;

import java.lang.annotation.*;

/**
 * The class Value.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/31 18:59
 */
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Value {

  String value();

}
