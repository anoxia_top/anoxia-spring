package cn.anoxia.springframework.beans.service.event;

import cn.anoxia.springframework.context.ApplicationListener;
import cn.anoxia.springframework.context.event.ContextClosedEvent;

import java.util.Date;

/**
 * The class ContextRefreshedEventListener.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 13:55
 */
public class ContextRefreshedEventListener implements ApplicationListener<ContextClosedEvent> {
  @Override
  public void onApplicationEvent(ContextClosedEvent event) {
    System.out.println("refresh的监听者监听到刷新消息："+new Date());
  }
}
