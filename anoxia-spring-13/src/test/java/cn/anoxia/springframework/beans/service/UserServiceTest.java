package cn.anoxia.springframework.beans.service;
import cn.anoxia.springframework.beans.BeansException;
import cn.anoxia.springframework.beans.dao.IUserDao;
import cn.anoxia.springframework.beans.support.ClassPathXmlApplicationContext;
import org.junit.jupiter.api.Test;
import org.openjdk.jol.info.ClassLayout;

import java.lang.reflect.Proxy;

import static org.junit.jupiter.api.Assertions.*;

/*
 * Copyright (C) 2011-2021 ShenZhen iBOXCHAIN Information Technology Co.,Ltd.
 *
 * All right reserved.
 *
 * This software is the confidential and proprietary
 * information of iBOXCHAIN Company of China.
 * ("Confidential Information"). You shall not disclose
 * such Confidential Information and shall use it only
 * in accordance with the terms of the contract agreement
 * you entered into with iBOXCHAIN inc.
 *
 */
class UserServiceTest {

  @Test
  public void test_prototype() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    System.out.println(applicationContext.getBean("proxyUserDao").toString());

    // 2. 获取Bean对象调用方法
    UserService userService1 = applicationContext.getBean("userService", UserService.class);
    UserService userService2 = applicationContext.getBean("userService", UserService.class);

    System.out.println(userService1);
    System.out.println(userService2);

    System.out.println(userService1+"十六进制哈希："+Integer.toHexString(Integer.parseInt(String.valueOf(userService1.hashCode()))));
    System.out.println(ClassLayout.parseInstance(userService1).toPrintable());
  }

  @Test
  public void test_proxy() throws BeansException {
    // 1.初始化 BeanFactory
    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
    // 注册销毁方法
    applicationContext.registerShutdownHook();

    UserService userService = applicationContext.getBean("userService", UserService.class);
    System.out.println(userService.queryUserInfo());
  }

  @Test
  public void test_proxy_class(){
    IUserDao instance = (IUserDao) Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), new Class[]{IUserDao.class}, (proxy, method, args) -> "你被代理了");
    System.out.println("测试结果："+instance.queryUserName("001"));

  }



}