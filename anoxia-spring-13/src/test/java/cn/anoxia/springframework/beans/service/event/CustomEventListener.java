package cn.anoxia.springframework.beans.service.event;

import cn.anoxia.springframework.context.ApplicationListener;
import cn.anoxia.springframework.context.event.ApplicationContextEvent;

import java.util.Date;

/**
 * The class CustomEventListener.
 * <p>
 * Description:
 *
 * @author: huangle
 * @since: 2021/08/30 11:27
 */
public class CustomEventListener implements ApplicationListener<CustomEvent> {
  @Override
  public void onApplicationEvent(CustomEvent event) {
    System.out.println("收到："+event.getSource()+"消息；时间："+ new Date());
    System.out.println("消息："+event.getId()+":"+event.getMessage());
  }
}
